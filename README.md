<!--
 ╭─────────────────────────────────────────╮
 │ NotoEmojiPlus OMF Extension | @MrCarb0n │
 │  gitlab.com/MrCarb0n/NotoEmojiPlus_OMF  │
 ├─────────────────────────────────────────┤
 │        Style: Google, Unicode 15        │
 │         Resources: @rkbdiemoji          │
 ├─────────────────────────────────────────┤
 │     credit: @OhMyFont, @RKBDI, MFFM!    │
 ╰─────────────────────────────────────────╯
-->

##### [how-to][1] :heavy_minus_sign: [**DOWNLOAD**][2] :heavy_minus_sign: [changelog][3]

[![NotoEmojiPlus_Banner](samples/banner.jpg)][4]

#### :nut_and_bolt: How-To-Install
- Save `notoemojiplus.zip` from download button.
- Extract to `OhMyFont` folder at internal storage.
  - `88_notoemojiplus.sh`
  - `notoemojiplus.xz`
- Re/Install `OhMyFont` module.
- Reboot.

#### :new: Changelog
- `v2022.10.23`
  - Merged `NotoEmojiPlus` update
  - Fixed typos in `README.md`

- `v2022.8.8`
  - Improved custom emoji support
  - Extended support upto API 33+
  
- `v2022.7.10`
  - Added custom emoji support
  - Misc codebase improvement

- `v2022.7.5`
  - Renamed resources to stock
  - Update codebase with OMF `v2022.07.0602`

- `v2022.5.30`
  - Noto Emojis based on Unicode 15
  - Additional unicode & symbol support
  - Included all OEMs support
  - Possible in-app emoji switch
  - A12+ extended support


#### :whale: Honour
- [RKBDI][5] / [XDA][6]
- [nongthaihoang][7] / [OhMyFont][8]
- [MFFM][9] / [Disc.][10]

[1]: #nut_and_bolt-how-to-install
[2]: https://gitlab.com/MrCarb0n/NotoEmojiPlus_OMF/-/raw/master/notoemojiplus.zip?inline=false
[3]: #new-changelog
[4]: https://gitlab.com/MrCarb0n/NotoEmojiPlus_OMF
[5]: https://t.me/RKBDI
[6]: https://forum.xda-developers.com/t/module-emoji-sets-by-rkbdi.4120991
[7]: https://gitlab.com/nongthaihoang
[8]: https://gitlab.com/nongthaihoang/oh_my_font
[9]: https://t.me/MFFMMain
[10]: https://t.me/MFFMDisc
