# ╭─────────────────────────────────────────╮
# │ NotoEmojiPlus OMF Extension | @MrCarb0n │
# │  gitlab.com/MrCarb0n/NotoEmojiPlus_OMF  │
# ├─────────────────────────────────────────┤
# │        Style: Google, Unicode 15        │
# │         Resources: @rkbdiemoji          │
# ├─────────────────────────────────────────┤
# │     credit: @OhMyFont, @RKBDI, MFFM!    │
# ╰─────────────────────────────────────────╯

${EMOJ:=false} && return

(   
    ui_print '  NotoEmojiPlus v2022.8.8'

    # get original system emoji filename
    [ $API -ge 33 ] &&
        S_EMJ="$(find $ORISYSFONT -type f \
                                  -iname '*emoji*legacy*.[ot]t[fc]' \
                                  -exec basename {} +)"
    [ $API -le 32 ] &&
        S_EMJ="$(find $ORISYSFONT -type f \
                                  -iname '*emoji*.[ot]t[fc]' \
                                ! -iname '*emoji*flags*.[ot]t[fc]' \
                                  -exec basename {} +)"

    # find custom emoji and copy to module dir
    C_EMJ="$(find $OMFDIR -type f \
                          -iname '*emoji*.[ot]t[fc]' \
                          -exec basename {} +)"
    [ $C_EMJ ] &&
        cp $OMFDIR/$C_EMJ $SYSFONT/$S_EMJ &&
        EXCLD="--exclude *[Ee]moji*.ttf" || unset EXCLD

    # extract NotoEmojiPlus resources
    [ -f $OMFDIR/notoemojiplus.xz ] &&
        tail -n +11 $OMFDIR/notoemojiplus.xz |
        tar xJf - $EXCLD -C $SYSFONT

    [ ! $C_EMJ ] &&
        mv -f $SYSFONT/NotoEmojiPlus.ttf $SYSFONT/$S_EMJ

    # Android 12+ | extended checking.. [?!]
    [ -d /data/fonts ] && rm -rf /data/fonts

    # clear cache data of Gboard
    [ -d /data/data/com.google.android.inputmethod.latin ] &&
        find /data -type d -path '*inputmethod.latin*/*cache*' \
                           -exec rm -rf {} + &&
        am force-stop com.google.android.inputmethod.latin

    # change possible in-app emojis on boot time
    [ ! -d $OMFDIR/service.d ] && mkdir -p $OMFDIR/service.d
    {
        echo '# ╭─────────────────────────────────────────╮'
        echo '# │ NotoEmojiPlus OMF Extension | @MrCarb0n │'
        echo '# │  gitlab.com/MrCarb0n/NotoEmojiPlus_OMF  │'
        echo '# ├─────────────────────────────────────────┤'
        echo '# │        Style: Google, Unicode 15        │'
        echo '# │         Resources: @rkbdiemoji          │'
        echo '# ├─────────────────────────────────────────┤'
        echo '# │     credit: @OhMyFont, @RKBDI, MFFM!    │'
        echo '# ╰─────────────────────────────────────────╯'
        echo ''
        echo '(   '
        echo '    until [ "$(resetprop sys.boot_completed)" = "1" -a -d "/data" ]; do'
        echo '        sleep 1'
        echo '    done'
        echo '    '
        echo '    [ -d /data/fonts ] && rm -rf /data/fonts'
        echo '    '
        echo '    find /data/data -type f -iname "*emoji*.[ot]t[fc]" -print |'
        echo '        while IFS= read -r EMJ; do'
        echo "            cp -f /system/fonts/$S_EMJ \$EMJ"
        echo '        done'
        echo ')'
    } > $OMFDIR/service.d/svc_notoemojiplus.sh

    EMOJ=true
    ver emoji
)
